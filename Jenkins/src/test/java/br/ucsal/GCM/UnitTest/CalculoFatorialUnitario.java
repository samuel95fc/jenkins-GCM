package br.ucsal.GCM.UnitTest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import br.ucsal.GCM.CalculoFatorial;

public class CalculoFatorialUnitario
{
	@Mock
	private CalculoFatorial Fatorial;
	
	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void FatorialCalculado()
	{
		Mockito.when(Fatorial.Fatorial(1L)).thenReturn(1L);
		Fatorial.Fatorial(1L);
		Mockito.verify(Fatorial).Fatorial(1L);
		
		Mockito.when(Fatorial.Fatorial(4L)).thenReturn(24L);
		Fatorial.Fatorial(4L);
		Mockito.verify(Fatorial).Fatorial(4L);
	}
}